


#include "../abstract/file_manager.h"

enum open_status open_file(const char* file_name, FILE** file, char* mode_open) {

    *file = fopen(file_name, mode_open);
    if (*file) {
        return OPEN_OK;
    }
    return OPEN_ERROR;

}

enum close_status close_file(FILE** file_stream) {

    if (file_stream) {
        fclose(*file_stream);
        return CLOSE_OK;
    };
    return CLOSE_ERROR;

}

