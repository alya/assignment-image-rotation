

#include "../abstract/image_builder.h"

struct image build_image(uint64_t width, uint64_t height) {

    return (struct image) {.width = width, .height = height, .data = malloc(3 * width * height)};

}

void image_destroy(struct image image) {
    free(image.data);
}

struct pixel get_pixel(struct image const* image ,uint64_t const offset, uint64_t const row) {
    struct pixel result = image->data[image->width * row + offset];
    return result;
}
void set_pixel(struct image* image ,uint64_t const offset, uint64_t const row ,struct pixel pixel) {
    image->data[image->width*row + offset] = pixel;
}
