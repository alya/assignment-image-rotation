
#include "../abstract/rotation.h"


struct image rotate( struct image const source ) {

    struct image result = build_image(source.height, source.width);
    for (uint64_t row = 0; row < source.height; row = row + 1) {
        for (uint64_t col = 0; col < source.width; col = col + 1) {
            set_pixel(&result, source.height - row - 1, col,get_pixel(&source, col, row));
        }
    }

    return result;

}
