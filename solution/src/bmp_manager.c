

#include "../abstract/bmp_manager.h"

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

static enum read_status check_header(struct bmp_header* header) {

    if (header->bfType != 0x4D42) {
        return READ_INVALID_TYPE;
    }
    if (header->bfReserved != 0) {
        return READ_INVALID_RESERVED;
    }
    if (header->bOffBits != 54) {
        return READ_INVALID_OFFBITS;
    }
    if (header->biSize != 40) {
        return READ_INVALID_SIZE;
    }
    if (header->biPlanes != 1) {
        return READ_INVALID_PLANES;
    }
    if (header->biBitCount != 24) {
        return READ_INVALID_BITSCOUNT;
    }
    return READ_OK;

}

static uint64_t get_padding(uint64_t width) {

    uint32_t padding = (4 - ((width * 3) % 4)) % 4;
    return padding;

}

static uint64_t get_file_size(uint64_t width, uint64_t height) {

    uint64_t size = (height * width * 3) + (height * get_padding(width)) + 54;
    return size;

}

static enum read_status read_header(FILE* file_name, struct bmp_header *header) {

    fread(header, sizeof(struct bmp_header), 1, file_name);
    if (check_header(header) == READ_OK) {
        return READ_OK;
    }
    return READ_INVALID_HEADER;

}

static struct bmp_header build_header(struct image const *image) {

    struct bmp_header header = {0};
    header.bfType = 0x4D42;
    header.bfileSize = get_file_size(image->width, image->height);
    header.bfReserved = 0;
    header.bOffBits = 54;
    header.biSize = 40;
    header.biWidth = image->width;
    header.biHeight = image->height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    return header;

}

enum read_status from_bmp(FILE* in, struct image* image) {

    if (!in) {
        return READ_INVALID_FILE;
    }

    struct bmp_header header;
    if (read_header(in, &header) != READ_OK) {
        return read_header(in, &header);
    }

    if (fseek(in, header.bOffBits, SEEK_SET) != 0) {
        return READ_INVALID_SIGNATURE;
    }

    *image = build_image(header.biWidth, header.biHeight);
    if (!image->data) {
        return READ_INVALID_SIGNATURE;
    }


    for (uint64_t row = 0; row < image->height; row = row + 1) {
        fread(image->data + (row * image->width), 3, image->width, in);
        if (fseek(in, (long) get_padding(image->width), SEEK_CUR) != 0) {
            free(image);
            return READ_INVALID_SIGNATURE;
        }

    }

    return READ_OK;

}

enum write_status to_bmp(FILE* out, struct image const* image) {

    if (!out) {
        return WRITE_ERROR;
    }

    struct bmp_header header = build_header(image);
    fwrite(&header, sizeof(struct bmp_header), 1, out);

    char* pad = calloc(get_padding(image->width), 1);
    //char pad[get_padding(image->width)];
    //char* array = calloc(sizeof(get_padding(image->width)),get_padding(image->width));
    for (uint64_t row = 0; row < image->height; row = row + 1) {
        fwrite(image->data + row * image->width, 3, image->width, out);
        fwrite(&pad, 1, get_padding(image->width), out);
        if (fseek(out, *pad, SEEK_CUR) != 0) {
            return WRITE_ERROR;
        }
    }
    free(pad);

    return WRITE_OK;

}
