
#include "../abstract/file_manager.h"
#include "../abstract/bmp_manager.h"
#include "../abstract/image_builder.h"
#include "../abstract/rotation.h"
#include <stdio.h>

#define ZERO 0
#define ARGUMENTS_ERROR 1
#define OPEN_FILE_ERROR 2
#define READ_IMAGE_ERROR 3
#define CLOSE_FILE_ERROR 4
#define WRITE_IMAGE_ERROR 5


int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    if (argc != 3) {
        return 1;
    }

    const char *input_file_name = argv[1];
    FILE* input;
    if (open_file(input_file_name, &input, "rb") != OPEN_OK) {
        return 2;
    }

    struct image source;
    if (from_bmp(input, &source) != READ_OK) {
        return 3;
    }

    if (close_file(&input) != CLOSE_OK) {
        return 4;
    }

    const char *output_file_name = argv[2];
    FILE* output;
    if (open_file(output_file_name, &output, "wb") != OPEN_OK) {
        return 2;
    }

    struct image result = rotate(source);
    free(source.data);

    if (to_bmp(output, &result) != WRITE_OK) {
        return 5;
    }

    image_destroy(result);

    if (close_file(&output) != CLOSE_OK) {
        return 4;
    }

    return 0;
}
