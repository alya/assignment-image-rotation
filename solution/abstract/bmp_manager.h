

#include <stdio.h>

#ifndef IMAGE_TRANSFORMER_BMPMANAGER_H
#define IMAGE_TRANSFORMER_BMPMANAGER_H


//  gcc & clang

#include "image_builder.h"
#include  <stdint.h>

/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_FILE,
    READ_INVALID_HEADER,
    READ_INVALID_TYPE,
    READ_INVALID_RESERVED,
    READ_INVALID_OFFBITS,
    READ_INVALID_SIZE,
    READ_INVALID_PLANES,
    READ_INVALID_BITSCOUNT
    /* another errors  */
};

enum read_status from_bmp( FILE* in, struct image* image );

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
    /* another errors  */
};

enum write_status to_bmp( FILE* out, struct image const* image );

#endif //IMAGE_TRANSFORMER_BMPMANAGER_H
