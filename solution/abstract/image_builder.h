
#include <malloc.h>
#include <stdint.h>


#ifndef IMAGE_TRANSFORMER_IMAGEBUILDER_H
#define IMAGE_TRANSFORMER_IMAGEBUILDER_H



struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image build_image(uint64_t width, uint64_t height);

void image_destroy(struct image image);

struct pixel get_pixel(struct image const* image ,uint64_t const offset, uint64_t const row);

void set_pixel(struct image* image ,uint64_t const offset, uint64_t const row ,struct pixel pixel);

#endif //IMAGE_TRANSFORMER_IMAGEBUILDER_H
