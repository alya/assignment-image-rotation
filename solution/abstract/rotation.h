

#ifndef IMAGE_TRANSFORMER_ROTATION_H
#define IMAGE_TRANSFORMER_ROTATION_H

#include "image_builder.h"


struct image rotate( struct image const source );

#endif //IMAGE_TRANSFORMER_ROTATION_H
