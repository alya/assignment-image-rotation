

#ifndef IMAGE_TRANSFORMER_FILEMANAGER_H
#define IMAGE_TRANSFORMER_FILEMANAGER_H

#include <stdio.h>

enum open_status  {
    OPEN_OK = 0,
    OPEN_ERROR
};

enum open_status open_file(const char* file_name, FILE** file, char* mode_open);

enum close_status {
    CLOSE_OK = 0,
    CLOSE_ERROR
};

enum close_status close_file(FILE** file_stream);

#endif //IMAGE_TRANSFORMER_FILEMANAGER_H

